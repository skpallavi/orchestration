#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"


/*typedef struct _simple_rec {
    double cpu_usage;
    int network_usage;
    char *name;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"cpu_usage", "double", sizeof(double), FMOffset(simple_rec_ptr, cpu_usage)},
    {"network_usage", "integer", sizeof(int), FMOffset(simple_rec_ptr, network_usage)},
    {"name", "string", sizeof(char*), FMOffset(simple_rec_ptr, name)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};*/


EVclient test_client;
char *my_name;

void adjust_network_bandwidth(int new_dbw, int new_ubw, char* new_command)
{
	char ndbw[5], nubw[5];
	snprintf(ndbw, 5,"%d",(new_dbw*10));
	snprintf(nubw, 5,"%d",(new_ubw*10));
	strcpy(new_command, "sudo wondershaper eth0 ");
	strcat(new_command, ndbw);
	strcat(new_command, " ");
	strcat(new_command, nubw);
}

void truncate_output(char output[])
{
       int i;
       int size = sizeof(output)/sizeof(output[0]);
       for(i=0; i<size;i++)
       {
               if(output[i] >= '0' && output[i] <= '9')
                       continue;
               else
               {
                       output[i] = '\0';
                       return;
               }
       }
}

void load_balancer_server(char *ip, char *port, char* sipp_command)
{
	strcpy(sipp_command, "sudo ./sipp -sn uas ");
	strcat(sipp_command, "-i "); 
	strcat(sipp_command, ip);
	strcat(sipp_command, " -p ");
	strcat(sipp_command, port);
	strcat(sipp_command, " -trace_stat -fd 1s -trace_rtt -rtt_freq 1");
	
}
void adjust_cpu_bandwidth(int new_limit, char* cpu_command)
{
	char nl[7];
	
	system("sudo killall -9 cpulimit");
		
	snprintf(nl, 5, "%d", new_limit);
	char *command = "pgrep sipp";
	char output[10];
	FILE *commf = popen(command, "r");
	if(commf == NULL)
	{
		printf(" unable to get sipp process id \n");
		exit(0);
	}
	fgets(output, 10, commf);
	pclose(commf);
	truncate_output(output);
	
	strcpy(cpu_command, "sudo cpulimit -p ");
	strcat(cpu_command, output);
	strcat(cpu_command, " -l ");
	strcat(cpu_command, nl);	
}

void prepare_prerouting_forward(char* current_ip,char* src_ip, char* port,char* dst_ip,char* command)
{
	strcpy(command, "sudo iptables -t nat -A PREROUTING -d ");
	strcat(command, current_ip);
	strcat(command, " -s ");
	strcat(command, src_ip);
	strcat(command, " -p udp --dport ");
	strcat(command, port);
	strcat(command, " -j DNAT --to-destination ");
	strcat(command, dst_ip);	
}

void prepare_postrouting_forward(char* src_ip, char* port, char* current_ip, char* command)
{
	strcpy(command, "sudo iptables -t nat -I POSTROUTING -s ");
	strcat(command, src_ip);
	strcat(command, " -p udp --dport ");
	strcat(command, port);
	strcat(command, " -j SNAT --to-source ");
	strcat(command, current_ip);
}

void prepare_destination_forward(char* dst_ip, char*port, char* command)
{
	strcpy(command, "sudo iptables -A FORWARD -d ");
	strcat(command, dst_ip);
	strcat(command, " -i eth0 -p udp --dport ");
	strcat(command, port);
	strcat(command, " -j ACCEPT");

}

void load_balancer_client(char *src_ip, char *dst_ip, char *port, char* sipp_command)
{
	
	strcpy(sipp_command, "sudo ./sipp -sf uac_pcap.xml ");
	strcat(sipp_command, dst_ip);
	strcat(sipp_command, ":");
	strcat(sipp_command, port);
	strcat(sipp_command, " -i ");
	strcat(sipp_command, src_ip);
	strcat(sipp_command, " -trace_stat -fd 1s -trace_rtt -rtt_freq 1");	
}

void kill_old_sipp_client()
{
	
	char *command = "pgrep sipp";
	char output[10];
	char *sipp_command = malloc(sizeof(char) *100);
	FILE *commf = popen(command, "r");
	if(commf == NULL)
	{
		printf(" unable to get sipp process id \n");
		exit(0);
	}
	fgets(output, 10, commf);
	pclose(commf);
	truncate_output(output);

	strcpy(sipp_command, "sudo kill -9 ");
	strcat(sipp_command, output);
	system(sipp_command);
}

static int
control_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_control_rec_ptr event = vevent;
    printf(" inside control handler : %s %s \n", event->command, event->action_node);
    if(strstr(my_name,event->action_node))
    {

       printf("Control command %s\n", event->command); 
       // Vandana and pallavi include changes here
       char* prepare_command = (char*) malloc(sizeof(char)*500);
       if (strcmp(event->command, "cpu_usage") == 0){ 
		adjust_cpu_bandwidth(event->cpu_usage, prepare_command);
		system(prepare_command);
	   }
       else if(strcmp(event->command, "net_bw") == 0){
		adjust_network_bandwidth(event->tx_bw, event->rx_bw, prepare_command);
		system("sudo wondershaper eth0 100000 100000");
	   }
    	else if(strcmp(event->command, "load_balance_server")==0)
	   {
			printf("triggering action for load_balance_server \n");
	   	 	load_balancer_server(event->src_ip, event->port, prepare_command);
			system(prepare_command);
	   }
	else if(strcmp(event->command, "load_balance_client")==0)
	   {		kill_old_sipp_client();
			printf("triggering action for load_balance_client \n");
	   		load_balancer_client(event->src_ip, event->dst_ip, event->port, prepare_command);
			system(prepare_command);
	   }
	else if(strcmp(event->command, "reroute_nat")==0)
	   {
			printf("triggering action for reroute nat \n");
	   		prepare_prerouting_forward(event->current_ip, event->src_ip, event->port,event->dst_ip, prepare_command);
			system(prepare_command);
	   		prepare_postrouting_forward(event->src_ip, event->port, event->current_ip, prepare_command);
			system(prepare_command);
	   		prepare_destination_forward(event->dst_ip, event->port, prepare_command);
			system(prepare_command);
	   }	
	else if(strcmp(event->command, "reroute_snort")==0)
	   {
			printf("triggering action for reroute_snort \n");
	   		prepare_prerouting_forward(event->current_ip, event->src_ip, event->port,event->dst_ip, prepare_command);
			system(prepare_command);	
	   		prepare_postrouting_forward(event->src_ip, event->port, event->current_ip, prepare_command);
			system(prepare_command);
	   		prepare_destination_forward(event->dst_ip, event->port, prepare_command);
			system(prepare_command);
	   }
	printf("prepare_command %s \n",prepare_command);
    } 	
    //EVclient_shutdown(test_client, event->integer_field == 318);
    return 1;
}

char* get_master_contact()
{
FILE *fp = fopen("master_contact.txt","r");
char *master = malloc(sizeof(char)*50);
if(fp != NULL)
{
   fscanf(fp,"%s",master);
    fclose(fp); 
}

return master;

}
int main(int argc, char **argv)
{
    CManager cm;
    char *str_contact;
    EVdfg_stone src, sink;
    EVsource source_handle;
    EVclient_sinks sink_capabilities;
    EVclient_sources source_capabilities;

    (void)argc; (void)argv;
    cm = CManager_create();
    CMlisten(cm);


    source_handle = EVcreate_submit_handle(cm, -1, simple_format_list);
    source_capabilities = EVclient_register_source("event source", source_handle);
    sink_capabilities = EVclient_register_sink_handler(cm, "control_handler", simple_control_format_list,
                                (EVSimpleHandlerFunc) control_handler, NULL);

    char *master = get_master_contact();
    my_name = malloc(sizeof(char)*30);
    strcpy(my_name,argv[1]);
    strcat(my_name," source control");

    test_client = EVclient_assoc(cm,my_name, master, source_capabilities, sink_capabilities);

    if (EVclient_ready_wait(test_client) != 1) {
        /* dfg initialization failed! */
        exit(1);
    }
    
        
	




/*! [Shutdown code] */
    if (EVclient_active_sink_count(test_client) > 0) {
        /* if there are active sinks, the handler will call EVclient_shutdown() */
    } else {
        if (EVclient_source_active(source_handle)) {
            /* we had a source and have already submitted, indicate success */
            EVclient_shutdown(test_client, 0 /* success */);
        } else {
            /* we had neither a source or sink, ready to shutdown, no opinion */
            EVclient_ready_for_shutdown(test_client);
        }
    }

    return(EVclient_wait_for_shutdown(test_client));
/*! [Shutdown code] */
}
     
