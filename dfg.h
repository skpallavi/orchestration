typedef struct node_control_source
{
	char* name;
	char *control_source_type;
	EVdfg_stone control_sink;
	char *sink_handler;

}node_control_source;
typedef struct dfg_node_source
{
	char *source_type;
	char *name;
	EVdfg_stone source;
	node_control_source* control;
}dfg_node_source;


typedef struct dfg_node_middle
{
	char *source_type;
	char *control_source_type;
	char *control_handler;
	char *sink_handler;
	char *name;
	int clink_count;
	EVdfg_stone source;
	EVdfg_stone control_source;
	EVdfg_stone control_sink;
	EVdfg_stone sink;
}dfg_node_middle;

typedef struct dfg_node_sink
{
	char *sink_handler;
	char *name;
	char *control_source_type;
	int clink_count;
	EVdfg_stone control_source;
	EVdfg_stone sink;
}dfg_node_sink;

typedef struct dfg
{
	EVdfg dfg;
	int num_sources;
	int num_middles;
	char * dfg_contact;
	dfg_node_source **sources;
	dfg_node_middle **middles;
	dfg_node_sink **sink;
}dfg;

typedef struct _simple_rec {
    double cpu_usage;
    double network_usage;
    double call_rate;
    int retransmissions;
    int failed_calls;
    double max_response_time;
    int watchdog_major;
    int watchdog_minor;
    char *name;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"cpu_usage", "double", sizeof(double), FMOffset(simple_rec_ptr, cpu_usage)},
    {"network_usage", "double", sizeof(double), FMOffset(simple_rec_ptr, network_usage)},
    {"call_rate", "double", sizeof(double), FMOffset(simple_rec_ptr, call_rate)},
    {"retransmissions", "integer", sizeof(int), FMOffset(simple_rec_ptr, retransmissions)},
    {"failed_calls", "integer", sizeof(int), FMOffset(simple_rec_ptr, failed_calls)},
    {"max_response_time", "double", sizeof(double), FMOffset(simple_rec_ptr, max_response_time)},
    {"watchdog_major", "integer", sizeof(int), FMOffset(simple_rec_ptr, watchdog_major)},
    {"watchdog_minor", "integer", sizeof(int), FMOffset(simple_rec_ptr, watchdog_minor)}, 
    {"name", "string", sizeof(char*), FMOffset(simple_rec_ptr, name)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};

typedef struct _simple_control_rec {
    
    char *command;
    double cpu_usage;
    double tx_bw;
    double rx_bw;
    char* src_ip;
    char* dst_ip;
    char* port;
    char* current_ip;
    char *action_node;
} simple_control_rec, *simple_control_rec_ptr;

static FMField simple_control_field_list[] =
{
   
    {"command", "string", sizeof(char*), FMOffset(simple_control_rec_ptr, command)},
    {"cpu_usage", "double", sizeof(double), FMOffset(simple_control_rec_ptr, cpu_usage)},
    {"tx_bw", "double", sizeof(double), FMOffset(simple_control_rec_ptr, tx_bw)},
    {"rx_bw", "double", sizeof(double), FMOffset(simple_control_rec_ptr, rx_bw)},
    {"src_ip", "string", sizeof(char*), FMOffset(simple_control_rec_ptr, src_ip)},
    {"dst_ip", "string", sizeof(char*), FMOffset(simple_control_rec_ptr, dst_ip)},
    {"port", "string", sizeof(char*), FMOffset(simple_control_rec_ptr, port)},
    {"current_ip", "string", sizeof(char*), FMOffset(simple_control_rec_ptr, current_ip)},
    {"action_node", "string", sizeof(char*), FMOffset(simple_control_rec_ptr, action_node)},
    {NULL, NULL, 0, 0}
};

static FMStructDescRec simple_control_format_list[] =
{
    {"simple_control", simple_control_field_list, sizeof(simple_control_rec), NULL},
    {NULL, NULL}
};
