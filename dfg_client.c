#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"


/*typedef struct _simple_rec {
    double cpu_usage;
    int network_usage;
    char *name;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"cpu_usage", "double", sizeof(double), FMOffset(simple_rec_ptr, cpu_usage)},
    {"network_usage", "integer", sizeof(int), FMOffset(simple_rec_ptr, network_usage)},
    {"name", "string", sizeof(char*), FMOffset(simple_rec_ptr, name)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};*/

#define rx_file "/sys/class/net/eth0/statistics/rx_bytes"
#define tx_file "/sys/class/net/eth0/statistics/tx_bytes"
#define cpu_file "/proc/stat"	

struct cpu_usage
{
  unsigned long total_time;
  double timestamp;

};

struct proc_cpu_usage
{
	double proc_user_time;
	double proc_system_time;
	unsigned long utime;
	unsigned long stime;
	double timestamp;
};

struct net_bandwidth
{
double total_bw;
double rx_bw;
double tx_bw;
long long old_tx_bytes;
long long old_rx_bytes;
double tx_time;
double rx_time;
};

typedef struct app_metrics{
	double call_rate;
        int retransmissions;
        int failed_calls;
        double max_response_time;
        int watchdog_major;
        int watchdog_minor;
}app_metrics;

//Calculating the network Bandwidth metric
long long get_bytes(char *filename, double* time);
double get_time();
struct net_bandwidth net_bw;
struct cpu_usage cpu_us;
struct proc_cpu_usage proc_cpu_us;
EVclient test_client;
char prev_sipp_client[6];

void truncate_output(char output[])
{
        int i;
        int size = sizeof(output)/sizeof(output[0]);
        for(i=0; i<size;i++)
        {
                if(output[i] >= '0' && output[i] <= '9')
                        continue;
                else
                {
                        output[i] = '\0';
                        return;
                }
        }
}

void first_token(char *cpu_string, int *index)
{
	char *c = cpu_string;
	while(*c < '0' || *c > '9')
	{
		c++;
		(*index)++;
	}
	printf(" %d index \n", *index);
	
}

unsigned long get_token(char *cpu_string, int *index)
{
	char *c = cpu_string + *index;
	unsigned long number = 0;
	while(*c >= '0' && *c <= '9')
	{
		number = number*10 + (*c)-'0';
		c++;
		(*index)++;
	}
	(*index)++;
	return number;
}

void get_cpu_string(char* filename, char **cpu_string, double *timestamp)
{
	FILE *fp;
	fp = fopen(filename, "r");
	size_t len=0;
	ssize_t read=0;

    	if(fp == NULL) 
	{
		printf("unable to open the file %s \n", filename); 
		exit(0);
    	}
	else
		printf(" file opened \n");
	
	read = getline(cpu_string, &len, fp);
	if(read == -1)
	{
		printf(" unable to read line \n");
		exit(0);
	}
	else
		printf(" %s \n", *cpu_string);
	*timestamp = get_time();
	fclose(fp);	
}

double retrieve_cpu_usage(char* cpu_string)
{
	unsigned long user_cpu;
  	unsigned long nice_cpu;
  	unsigned long system_cpu;
  	unsigned long idle_cpu;
  	unsigned long wio_cpu;
  	unsigned long irq_cpu;
  	unsigned long sirq_cpu;
	int index = 0;

	/*printf(" received cpu string : %s \n", cpu_string);*/

	first_token(cpu_string, &index);

	user_cpu = get_token(cpu_string, &index);
	nice_cpu = get_token(cpu_string, &index);
	system_cpu = get_token(cpu_string, &index);
	idle_cpu = get_token(cpu_string, &index);
	wio_cpu = get_token(cpu_string, &index);
	irq_cpu = get_token(cpu_string, &index);
	sirq_cpu = get_token(cpu_string, &index);

	
	
	 return user_cpu + nice_cpu + system_cpu + idle_cpu + wio_cpu + irq_cpu + sirq_cpu;

}

void retrieve_proc_cpu_usage(char *proc_string, unsigned long total_time, double proc_timestamp, double cpu_timestamp)
{
	unsigned long utime;
	unsigned long stime;
	const char* tok;
	int token_num=0;
	for(tok = strtok(proc_string, " ");tok&&*tok; tok=strtok(NULL, " \n"))
	{
		token_num++;
		if(token_num == 14)
		{
			utime = atol(tok);
//			printf("%s %ld %d utime \n", tok, utime, token_num);
		}
		if(token_num == 15)
		{
			stime = atol(tok);
//			printf("%s %ld %d stime \n", tok, stime,token_num);
		}
		if(token_num >=16)
			break;
	}
	//printf("%ld totaltime-old total time, %d utime - old utime \n", total_time -cpu_us.total_time, utime - proc_cpu_us.utime);
	proc_cpu_us.proc_user_time = (((utime - proc_cpu_us.utime)*100.00/(total_time -cpu_us.total_time)))/(proc_timestamp - proc_cpu_us.timestamp);
	proc_cpu_us.proc_system_time = ((stime - proc_cpu_us.stime)*100.00/(total_time -cpu_us.total_time))/(cpu_timestamp - cpu_us.timestamp);
	proc_cpu_us.utime = utime;
	proc_cpu_us.stime = stime;
	
}

double get_cpu_usage()
{
	char *cpu_string = (char *)malloc(150*sizeof(char));
	unsigned long total_time;
	double proc_timestamp, cpu_timestamp;
	char *proc_string = (char *)malloc(500*sizeof(char));

        char proc_file[25];
	strcpy(proc_file, "/proc/");
	char* command = "pgrep sipp";
	char output[10];
	FILE *commf = popen(command, "r");
	if(commf == NULL)
	{
		printf(" unable to get sipp process id \n");
		exit(0);
	}

	fgets(output, 10, commf);
	pclose(commf);

	truncate_output(output);

	strcat(proc_file, output);

	strcat(proc_file, "/stat");	

	get_cpu_string(proc_file, &proc_string, &proc_timestamp);
//	printf( " proc_string inside get_cpu_usage : %s \n", proc_string);

	get_cpu_string(cpu_file, &cpu_string, &cpu_timestamp);
//	printf( " cpu_string inside get_cpu_usage : %s \n", cpu_string);
	total_time = retrieve_cpu_usage(cpu_string);	
	retrieve_proc_cpu_usage(proc_string, total_time,proc_timestamp, cpu_timestamp);
	cpu_us.total_time = total_time;
	cpu_us.timestamp = cpu_timestamp;
	proc_cpu_us.timestamp = proc_timestamp;
//	printf(" proc_time %f %f \n", proc_cpu_us.proc_user_time , proc_cpu_us.proc_system_time);
	return (proc_cpu_us.proc_system_time+proc_cpu_us.proc_user_time);
}

void initiate_cpu_structs()
{
	cpu_us.total_time=0;
	proc_cpu_us.utime = 0;
	proc_cpu_us.stime = 0;
	double cpu_usage = get_cpu_usage();
}

void initiate_bytes(struct net_bandwidth* net_bw)
{
    net_bw->old_rx_bytes = get_bytes(rx_file, &(net_bw->rx_time));
    net_bw->old_tx_bytes = get_bytes(tx_file, &(net_bw->tx_time));
}

long long get_bytes(char *filename, double* time)
{
    FILE *fp;
    long long bytes;

    fp = fopen(filename, "r");


        if(fp == NULL) 
    {
        printf("unable to open the file %s \n", filename); 
        }
    
    fscanf(fp, "%lld", &bytes);
    *time = get_time();
    fclose(fp);
    return bytes;
}

double get_time()
{
    double time_in_sec;
    struct timeval now;
        gettimeofday(&now, NULL);
        time_in_sec = now.tv_sec + now.tv_usec* 1.0e-6 ;
    return time_in_sec;
}

void calculate_bandwidth(struct net_bandwidth* net_bw)
{
    double new_tx_time;
    double new_rx_time;
    long long rx_bytes, tx_bytes;
    
    rx_bytes = get_bytes(rx_file, &new_rx_time);
    tx_bytes = get_bytes(tx_file, &new_tx_time);
    net_bw->rx_bw = ((rx_bytes - net_bw->old_rx_bytes) / (new_rx_time - net_bw->rx_time))/1000;
    net_bw->tx_bw = ((tx_bytes - net_bw->old_tx_bytes) / (new_tx_time - net_bw->tx_time))/1000;
        
    net_bw->total_bw = net_bw->rx_bw + net_bw->tx_bw;
    net_bw->old_rx_bytes = rx_bytes;
    net_bw->old_tx_bytes = tx_bytes;
    net_bw->rx_time = new_rx_time;
    net_bw->tx_time = new_tx_time;      
}

//Getting the Application level metrics
app_metrics getAppMetrics(FILE** fp, FILE** fp_rtt){
        int successful_calls=0;
        char* line=NULL;
        size_t len = 0;
        ssize_t read;
        app_metrics metrics;
        if ((read = getline(&line, &len, *fp)) != -1) {
                printf("Retrieved line of length %zu :\n", read);
                printf("%s", line);
                char* tmp = strdup(line);
                const char* tok;
                int token_num=0;
                for(tok=strtok(line, ";"); tok&&*tok; tok=strtok(NULL, ";\n")){
                        token_num++;
						printf("token_num::%d\n", token_num);
                        switch(token_num)
                        {
                                case 7:   metrics.call_rate = atof(tok);break;
                                case 49:  metrics.retransmissions = atoi(tok);break;
                                case 17:  metrics.failed_calls = atoi(tok); printf("failed calls from csv::%d\n", atoi(tok));break;
                                case 15:  successful_calls = atoi(tok);break;
                                          /*int i=0;
                                          char* line_rtt=NULL;
                                          size_t len_rtt = 0;
                                          ssize_t read_rtt;
                                          metrics.max_response_time = 0.0;
                                          for(i=0; i<successful_calls; i++){
                                                 if((read_rtt=getline(&line_rtt, &len_rtt, *fp_rtt))!=-1){
                                                        printf("Retrieved line of length rtt %zu :\n", read_rtt);
                                                        printf("%s", line_rtt);
                                                        const char* tok_rtt;
                                                        int token_num_rtt=0;
                                                        for(tok_rtt=strtok(line_rtt, ";"); tok_rtt&&*tok_rtt; tok_rtt=strtok(NULL, ";\n")){
                                                                token_num_rtt++;
                                                                if(token_num_rtt == 2){
                                                                        if(metrics.max_response_time < atof(tok_rtt))
                                                                                metrics.max_response_time = atof(tok_rtt);
                                                                }
                                                        }
                                                }
                                          }
                                          break;*/
                                case 57:  metrics.watchdog_major = atoi(tok);break;
                                case 59:  metrics.watchdog_minor = atoi(tok);break;
                                default:  break;
                        }
                }
        }
        int i=0;
        char* line_rtt=NULL;
        size_t len_rtt = 0;
        ssize_t read_rtt;
        metrics.max_response_time = 0.0;
        for(i=0; i<successful_calls; i++){
        	if((read_rtt=getline(&line_rtt, &len_rtt, *fp_rtt))!=-1){
            	printf("Retrieved line of length rtt %zu :\n", read_rtt);
            	printf("%s", line_rtt);
            	const char* tok_rtt;
            	int token_num_rtt=0;
            	for(tok_rtt=strtok(line_rtt, ";"); tok_rtt&&*tok_rtt; tok_rtt=strtok(NULL, ";\n")){
            		token_num_rtt++;
                	if(token_num_rtt == 2){
                		if(metrics.max_response_time < atof(tok_rtt))
                    		metrics.max_response_time = atof(tok_rtt);
                	}
            	}
        	}
        }
        return metrics;
}

void getuacFile(FILE** fp, FILE** fp_rtt){
       FILE* commf;
       char * command = "pgrep sipp";
       char output[10];

    // Setup our pipe for reading and execute our command.
       commf = popen(command,"r");

    // Get the data from the process execution
       fgets(output, 10, commf);
       pclose(commf);
       truncate_output(output);
       strcpy(prev_sipp_client, output);
       char filename[80];
       char filename_rtt[80];
       strcpy(filename, "/home/ubuntu/sipp-3.3.990/uac_pcap_");
       strcat(filename, output);
       strcpy(filename_rtt, filename);
       strcat(filename, "_.csv");
       strcat(filename_rtt, "_rtt.csv");
       printf("%s\n", filename);
       printf("%s\n", filename_rtt);
       *fp = fopen(filename, "r");
       *fp_rtt = fopen(filename_rtt, "r");
       if (*fp != NULL){
                char* line;
                size_t len= 0;
                getline(&line, &len, *fp);
        }
       else
         printf(" file opening error \n");
       if(*fp_rtt != NULL){
                char* line;
                size_t len= 0;
                getline(&line, &len, *fp_rtt);
       }
        else
         printf("file_rtt opening error\n");
}

static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr event = vevent;
    printf("I got %lf\n", event->cpu_usage);
    //EVclient_shutdown(test_client, event->integer_field == 318);
    return 1;
}

char* get_master_contact()
{
FILE *fp = fopen("master_contact.txt","r");
char *master = malloc(sizeof(char)*50);
if(fp != NULL)
{
   fscanf(fp,"%s",master);
    fclose(fp); 
}

return master;

}

void get_current_sipp_client( char* pid)
{
	FILE* commf;
       char * command = "pgrep sipp";
       char output[10];

    // Setup our pipe for reading and execute our command.
       commf = popen(command,"r");

    // Get the data from the process execution
       fgets(output, 10, commf);
       pclose(commf);
       truncate_output(output);

       strcpy(pid, output);
}

int main(int argc, char **argv)
{
    CManager cm;
    char *str_contact;
    EVdfg_stone src, sink;
    EVsource source_handle;
    EVclient_sinks sink_capabilities;
    EVclient_sources source_capabilities;

    (void)argc; (void)argv;
    cm = CManager_create();
    CMlisten(cm);


    source_handle = EVcreate_submit_handle(cm, -1, simple_format_list);
    source_capabilities = EVclient_register_source("event source", source_handle);
    sink_capabilities = EVclient_register_sink_handler(cm, "simple_handler", simple_format_list,
                                (EVSimpleHandlerFunc) simple_handler, NULL);

    char *master = get_master_contact();
    char *my_name = malloc(sizeof(char)*20);
    strcpy(my_name,argv[1]);
    strcat(my_name," source");


    test_client = EVclient_assoc(cm,my_name, master, source_capabilities, sink_capabilities);

    if (EVclient_ready_wait(test_client) != 1) {
        /* dfg initialization failed! */
        exit(1);
    }
    
        double cpu;
        initiate_bytes(&net_bw);
	    initiate_cpu_structs(&cpu_us);
	
     	FILE* fp;
        FILE* fp_rtt;
        getuacFile(&fp, &fp_rtt);
	if(fp != NULL)
	{
		fseek(fp, 0, SEEK_END);
		fseek(fp_rtt, 0, SEEK_END);
	}
		
        app_metrics metrics;
	char current_sipp_client[6];

        while(1)
        {
            if (EVclient_source_active(source_handle)) 
        {
            //FILE *fp = fopen("cpu.txt","r");
            //fscanf(fp,"%d",cpu);
        cpu = get_cpu_usage();
        calculate_bandwidth(&net_bw);
        printf("net bandwidth %f \n", net_bw.total_bw);
	    
	
        simple_rec rec;
        rec.cpu_usage = cpu;
        rec.network_usage=net_bw.total_bw; 
        rec.name=my_name;
	get_current_sipp_client(current_sipp_client);


	if(strstr(my_name, "sipp") != NULL)
        {
		if(strcmp(prev_sipp_client, current_sipp_client) != 0)
		{
			getuacFile(&fp, &fp_rtt);
			if(fp != NULL)
			{
				fseek(fp, 0, SEEK_END);
				fseek(fp_rtt, 0, SEEK_END);
			}		
		}
	}
        if(fp!=NULL){
            if(strstr(my_name, "sipp")!= NULL){
            	metrics = getAppMetrics(&fp,&fp_rtt);
            	if(strstr(my_name, "sipp_server") != NULL){
            		rec.call_rate=metrics.call_rate;
            		rec.retransmissions=0;
            		rec.failed_calls=0;
            		rec.watchdog_major=0;
            		rec.watchdog_minor=0;
            		rec.max_response_time = 0.0;
            	}
            	else{
            	        rec.retransmissions=metrics.retransmissions;
            		rec.failed_calls=metrics.failed_calls;
            		printf("failed calls while putting in rec::%d\n", rec.failed_calls);
            		rec.watchdog_major=metrics.watchdog_major;
            		rec.watchdog_minor=metrics.watchdog_minor;
            		rec.max_response_time=metrics.max_response_time;
            	}
            }
        }
        else{
            printf("FILE NOT FOUND\n");
            rec.call_rate=0;
            rec.retransmissions=0;
            rec.failed_calls=0;
            rec.max_response_time=0.0;
            rec.watchdog_major=0;
            rec.watchdog_minor=0;
	}
        net_bw.total_bw = 0;

        /* submit would be quietly ignored if source is not active */
        printf("Submitting source %s and cpu :%lf and nw : %lf and failed_calls : %d and retransmissions: %d\n",my_name,rec.cpu_usage,rec.network_usage, rec.failed_calls, rec.retransmissions);
        EVsubmit(source_handle, &rec, NULL);
        sleep(1);
        }

        }

        fclose(fp);
        fclose(fp_rtt);

	/*! [Shutdown code] */
    if (EVclient_active_sink_count(test_client) > 0) {
        /* if there are active sinks, the handler will call EVclient_shutdown() */
    } else {
        if (EVclient_source_active(source_handle)) {
            /* we had a source and have already submitted, indicate success */
            EVclient_shutdown(test_client, 0 /* success */);
        } else {
            /* we had neither a source or sink, ready to shutdown, no opinion */
            EVclient_ready_for_shutdown(test_client);
        }
    }

    return(EVclient_wait_for_shutdown(test_client));
/*! [Shutdown code] */
}
         
