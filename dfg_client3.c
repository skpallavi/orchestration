#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"
/*typedef struct _simple_rec {
    double cpu_usage;
    int network_usage;
    char *name;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"cpu_usage", "double", sizeof(double), FMOffset(simple_rec_ptr, cpu_usage)},
    {"network_usage", "integer", sizeof(int), FMOffset(simple_rec_ptr, network_usage)},
    {"name", "string", sizeof(char*), FMOffset(simple_rec_ptr, name)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};*/

EVclient test_client;
EVsource source_handle,source_handle2;
EVclient_sources source_capabilities;
char** nodes_i_can_reach;
int known_neighbours=0;
int total_nodes;
static int
middle_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{ static int count=0;
    simple_rec_ptr event = vevent;
    printf("I am in the middle and I got cpu :%lf || nw:%lf || failed_calls:%d || max_response_time:%lf from %s \n", event->cpu_usage,event->network_usage,event->failed_calls,event->max_response_time,event->name);
    //Finding out nodes I can reach
    int i;
    if (count==0)
        {
            
            char *neighbour=malloc(strlen(event->name));
            strcpy(neighbour,event->name);
            nodes_i_can_reach[0]=neighbour;
            known_neighbours=1;
            printf("I can reach %s\n",neighbour);

        }
    else if(known_neighbours<=total_nodes)    
        {
            int match=0;
            for(i=0;i<known_neighbours;i++)
            { 
                if(strcmp(nodes_i_can_reach[i],event->name)==0)
                    match=1;

            }
            if(match==0)
            {
                char *neighbour=malloc(strlen(event->name));
                strcpy(neighbour,event->name);
                nodes_i_can_reach[known_neighbours]=neighbour;
                known_neighbours++;
                printf("I can reach %s\n",neighbour);


            }

        }

    /* submit would be quietly ignored if source is not active */
    simple_rec_ptr rec = (simple_rec_ptr)malloc(sizeof(simple_rec));
    rec->cpu_usage = event->cpu_usage;
    rec->network_usage= event->network_usage;
    rec->call_rate = event->call_rate;
    rec->retransmissions = event->retransmissions;  
    rec->failed_calls=event->failed_calls;
    rec->max_response_time=event->max_response_time;
    rec->watchdog_minor=event->watchdog_minor;
    rec->watchdog_major=event->watchdog_major;
    rec->name=strdup(event->name);  
    EVsubmit(source_handle2, rec, NULL);
    count++;
    //if(count==2)
    //EVclient_shutdown(test_client, TRUE);
    return 1;
}

static int control_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_control_rec_ptr cmd;
    cmd=vevent;
    int i=0;
    int match=0;
    printf("%s %s\n",cmd->action_node,cmd->command);
    for(i=0;i<known_neighbours;i++)
    {
        if(strstr(nodes_i_can_reach[i],cmd->action_node) != NULL)
                    match=1;
    }
    if(match==1)
{
   printf("its a match so submitting forward\n");
   EVsubmit(source_handle,cmd, NULL);
}
}

char* get_master_contact()
{
FILE *fp = fopen("master_contact.txt","r");
char *master = malloc(sizeof(char)*50);
if(fp != NULL)
{
   fscanf(fp,"%s",master);
    fclose(fp); 
}
return master;

}


int main(int argc, char **argv)
{
    CManager cm;
    char *str_contact;
    EVdfg_stone src, sink;
    //EVsource source_handle;
    EVclient_sinks sink_capabilities;
    

   
    cm = CManager_create();
    CMlisten(cm);

/*
**  LOCAL DFG SUPPORT   Sources and sinks that might or might not be utilized.
*/
//printf("middle 1 \n");
    source_handle = EVcreate_submit_handle(cm, -1, simple_control_format_list);
    source_handle2 = EVcreate_submit_handle(cm, -1, simple_format_list);
    source_capabilities = EVclient_register_source("control source", source_handle);
    source_capabilities = EVclient_register_source("middle source", source_handle2);
    sink_capabilities = EVclient_register_sink_handler(cm, "middle_handler", simple_format_list,
                                (EVSimpleHandlerFunc) middle_handler, NULL);
    sink_capabilities = EVclient_register_sink_handler(cm, "control_handler", simple_control_format_list,
                                (EVSimpleHandlerFunc) control_handler, NULL);
    nodes_i_can_reach=malloc(sizeof(char*)*atoi(argv[2]));

total_nodes=atoi(argv[2]);
    /* We're node argv[1] in the process set, contact list is argv[2] */
    char *master = get_master_contact();
    char *my_name = malloc(sizeof(char)*20);
    strcpy(my_name,argv[1]);
    strcat(my_name," middle");
    test_client = EVclient_assoc(cm,my_name, master, source_capabilities, sink_capabilities);
printf(" Middle is here\n");
    if (EVclient_ready_wait(test_client) != 1) {
        /* dfg initialization failed! */
        exit(1);
    }
    

/*! [Shutdown code] */
    // if (EVclient_active_sink_count(test_client) > 0) {
        /* if there are active sinks, the handler will call EVclient_shutdown() */
    //  } else {
    //    if (EVclient_source_active(source_handle)) {
            /* we had a source and have already submitted, indicate success */
    //        EVclient_shutdown(test_client, 0 /* success */);
    //    } else {
            /* we had neither a source or sink, ready to shutdown, no opinion */
    //        EVclient_ready_for_shutdown(test_client);
    //    }
    //    }   

    return(EVclient_wait_for_shutdown(test_client));
/*! [Shutdown code] */
}
