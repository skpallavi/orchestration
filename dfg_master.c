#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"
#include<sys/time.h>
#define QOS_THRESHOLD 70

dfg *dfg_obj;
EVclient test_client;
EVsource source_handle;

EVclient_sources source_capabilities;
char** nodes_i_can_reach;
int num_neighbours;
simple_rec_ptr prev_events[20];
int num_nodes=0;
void store_as_previous_event(void *vevent);
int find_previous_event(void *vevent);
int action_taken =0;
double action_timestamp=0.0;
double current_time;
double server_call_rate=0.0;
int levels,total_nodes;
int loadbalancer =0;
double get_time()
{
    double time_in_sec;
    struct timeval now;
        gettimeofday(&now, NULL);
        time_in_sec = now.tv_sec + now.tv_usec* 1.0e-6 ;
    return time_in_sec;
}

int find_previous_event(void *vevent)
{
	simple_rec_ptr event = vevent;
	int i;
	int found = -1;
	for(i=0; i < num_nodes; i++)
	{
		if(strcmp(prev_events[i]->name, event->name) == 0)
		{
			found = i;
			break;
		}
	}
	return found;
}

void store_as_previous_event(void *vevent)
{
	int found = find_previous_event(vevent);
	if(found == -1)
	{
		prev_events[num_nodes++] = (simple_rec_ptr)vevent;
	}
	else
		prev_events[found] = (simple_rec_ptr)vevent;
}

void find_nodes(int nodes[], char *compare_string)
{
	int num_matches = 0; 
	int index=1;
	int i;
	for(i=0; i < num_nodes; i++)
	{
		if(strstr(prev_events[i]->name, compare_string) != NULL)
		{
			printf(" found node event %s in list of nodes %s\n", compare_string,prev_events[i]->name);
			num_matches++;
			nodes[index++] = i;
		}
	}
	nodes[0] = num_matches;
}

double cpu_usage_limit(double call_rate, char* name)
{
	if(strstr(name, "sipp_server")!=NULL)
	{
		if(call_rate<=30)
			return 7.0;
		if(call_rate<=60)
			return 10.0;
		if(call_rate <=90)
			return 13.0;
		if(call_rate <=120)
			return 16.0;
		if(call_rate<=150)
			return 21.0;
		if(call_rate <=180)
			return 25.0;
		if(call_rate <=210)
			return 30.0;
		if(call_rate <=240)
			return 37.0;
		if(call_rate <=270)
			return 43.0;
		if(call_rate <=300)
			return 45.0;	
	}

	if((strstr(name, "snort") != NULL) || (strstr(name, "nat") != NULL))
	{
		return 5.0;
	}
	return 30.0;
}

double net_bw_limit(double call_rate, char* name)
{
	double target_bw = 0.0;

	if(strstr(name, "sipp_server") != NULL)
	{
		if(call_rate <= 30)
			return 1305;
		if(call_rate <= 60)
			return 2612;
		if(call_rate <= 90)
			return 3915;
		if(call_rate <=120)
			return 5216;
		if(call_rate <= 150)
			return 6537;
		if(call_rate <= 180)
			return 7832;
		if(call_rate <= 210)
			return 9125;
		if(call_rate <= 240)
			return 10443;
		if(call_rate <= 270)
			return 11736;
		if(call_rate <= 300)
			return 13050;
	}
	
	if(strstr(name, "snort") != NULL)
	{
		if(call_rate <= 30)
			return 59;
		if(call_rate <= 60)
			return 123;
		if(call_rate <= 90)
			return 173;
		if(call_rate <=120)
			return 230;
		if(call_rate <= 150)
			return 620;
		if(call_rate <= 180)
			return 700;
		if(call_rate <= 210)
			return 760;
		if(call_rate <= 240)
			return 830;
		if(call_rate <= 270)
			return 860;
		if(call_rate <= 300)
			return 950;	
	}

	if(strstr(name, "nat") != NULL)
	{
		if(call_rate <= 30)
			return 173;
		if(call_rate <= 60)
			return 355;
		if(call_rate <= 90)
			return 529;
		if(call_rate <=120)
			return 701;
		if(call_rate <= 150)
			return 874;
		if(call_rate <= 180)
			return 900;
		if(call_rate <= 210)
			return 1100;
		if(call_rate <= 240)
			return 1400;
		if(call_rate <= 270)
			return 1573;
		if(call_rate <= 300)
			return 1749;	
	}
	return 1500.0;
}

void adjust_cpu_usage(char* node_name, double cpu_usage)
{
	simple_control_rec cmd;
    cmd.command="cpu_usage";
    cmd.action_node=node_name;
    cmd.cpu_usage = cpu_usage;
    cmd.tx_bw=0;
    cmd.rx_bw=0;
    printf("adjust cpu usage command : %s , %f \n", node_name, cpu_usage);
    cmd.src_ip = NULL;
    cmd.dst_ip = NULL;
    cmd.port = NULL;
    cmd.current_ip = NULL;
    action_timestamp = current_time;
    EVsubmit(source_handle,&cmd, NULL);

}

void adjust_net_bw(char* node_name, double target_net_bw){
	simple_control_rec cmd;
    cmd.command="net_bw";
    cmd.action_node=node_name;
    cmd.cpu_usage = 0;
    cmd.tx_bw=target_net_bw/2;
    cmd.rx_bw=target_net_bw/2;
	
    printf("adjust net_bw usage command : %s , %f \n", node_name, target_net_bw);
    cmd.src_ip = NULL;
    cmd.dst_ip = NULL;
    cmd.port = NULL;
    cmd.current_ip = NULL;
    action_timestamp = current_time;
    EVsubmit(source_handle,&cmd, NULL);
}

double get_server_max_callrate()
{
	int nodes[10];
	find_nodes(nodes, "sipp_server");
	int i;
	double max_call_rate = 0.0;
			
	for(i = 1; i <= nodes[0]; i ++)
	{	
				
		if(max_call_rate < (prev_events[nodes[i]]->call_rate))
			max_call_rate = prev_events[nodes[i]]->call_rate;
	}

	return max_call_rate;
}
	
void adjust_system_parameters(void *vevent)
{
	simple_rec_ptr event = vevent;
	printf(" inside adjust_system_parameters, node_name : %s \n", event->name);
	double target_cpu_usage = cpu_usage_limit(server_call_rate, event->name);
	double target_net_bw = net_bw_limit(server_call_rate, event->name);

	if(event->cpu_usage < target_cpu_usage)
		adjust_cpu_usage(event->name, target_cpu_usage);
	if(event->network_usage < target_net_bw)
		adjust_net_bw(event->name, target_net_bw);
}

void adjust_parameters_all_nodes(simple_rec_ptr event)
{
	adjust_parameters("sipp_server", event);
	adjust_parameters("nat" , event);
	adjust_parameters("snort" , event);		
}

void adjust_parameters(char* compare_string, simple_rec_ptr event)
{
	int nodes[10];
	find_nodes(nodes, compare_string);
	int i;
			
	for(i = 1; i <= nodes[0]; i ++)
	{				
		adjust_system_parameters(prev_events[nodes[i]]);
	}
}

char* find_heaviest_client()
{
	int clients[10];
	find_nodes(clients, "sipp_client");
	int i;
	double max_call_rate=0.0;
	int max_call_rate_index = 0;
	for(i=1; i<= clients[0]; i++)
	{
		if(prev_events[clients[i]]->call_rate > max_call_rate)
		{
			max_call_rate = prev_events[clients[i]]->call_rate;
			max_call_rate_index = clients[i];
		}		
	}
	
	return (max_call_rate_index >0) ? prev_events[max_call_rate_index]->name : NULL;
}


void invoke_load_balancer_server(){
	simple_control_rec cmd;
    cmd.command="load_balance_server";
    cmd.action_node = "sipp_server_b";
    cmd.cpu_usage = 0;
    cmd.tx_bw=0;
    cmd.rx_bw=0;
    cmd.src_ip = "192.168.111.110";
    cmd.dst_ip = NULL;
    cmd.port = "5061";
    cmd.current_ip = NULL;
    printf(" load_balance_server \n");
    EVsubmit(source_handle,&cmd, NULL);	
}

void reroute_sipp_traffic(){
	char* client = find_heaviest_client();
	char client_file[80];
	strcpy(client_file, "client_conf.txt");
	FILE* fp = fopen(client_file, "r");
	if (fp != NULL){
		char* line=NULL;
        size_t len = 0;
        ssize_t read;
	printf(" successfully opened client_conf.txt");
        if ((read = getline(&line, &len, fp)) != -1) {
        	printf("%s\n", line);
        	char* tmp = strdup(line);
            const char* tok;
            for(tok=strtok(tmp, "->"); tok&&*tok; tok=strtok(NULL, "->\n")){
            	if(strstr(tok, "nat")!=NULL ){
            		simple_control_rec cmd;
			printf(" token : %s \n", tok);
    				cmd.command="reroute_nat";
    				cmd.action_node = "nat";
    				cmd.cpu_usage = 0;
    				cmd.tx_bw=0;
    				cmd.rx_bw=0;
    				cmd.src_ip = heaviest_client_ip;
    				cmd.dst_ip = snort_ip;
    				cmd.port = "5061";
    				cmd.current_ip = nat_ip;
				printf(" reroute_nat \n");
    				EVsubmit(source_handle,&cmd, NULL);	
            	}
            	if(strstr(tok, "snort")!=NULL ){
            		simple_control_rec cmd;
			printf(" token : %s \n", tok);
    				cmd.command="reroute_snort";
    				cmd.action_node = "snort";
    				cmd.cpu_usage = 0;
    				cmd.tx_bw=0;
    				cmd.rx_bw=0;
    				cmd.src_ip = nat_ip;
    				cmd.dst_ip = new_server_ip;
    				cmd.port = "5061";
    				cmd.current_ip = snort_ip;
				printf("reroute snort \n");
    				EVsubmit(source_handle,&cmd, NULL);	
            	}
            }
        }
	}	
	else
	  printf("unable to open client_conf.txt \n");
}

void invoke_load_balancer_client(char* heaviest_client){
	simple_control_rec cmd;
    cmd.command="load_balance_client";
    cmd.action_node = heaviest_client;
    cmd.cpu_usage = 0;
    cmd.tx_bw=0;
    cmd.rx_bw=0;
    cmd.src_ip = heaviest_client_ip;
    cmd.dst_ip = nat_ip;
    cmd.port = "5061";
    cmd.current_ip = NULL;
    printf(" load_balace_client \n");
    EVsubmit(source_handle,&cmd, NULL);	
}


static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    static int count=0,known_neighbours=0;
    int i ;
    simple_rec_ptr event = malloc(sizeof(simple_rec));
    simple_rec_ptr vevent_ptr = vevent;
    event->name = malloc(strlen(vevent_ptr->name));
    count++;

   event->cpu_usage = vevent_ptr->cpu_usage;
   event->network_usage = vevent_ptr->network_usage;
   event->retransmissions = vevent_ptr->retransmissions;
   event->failed_calls = vevent_ptr->failed_calls;
   event->max_response_time = vevent_ptr->max_response_time;
   event->watchdog_major = vevent_ptr->watchdog_major;
   event->watchdog_minor = vevent_ptr->watchdog_minor;
   event->call_rate = vevent_ptr->call_rate;
   strcpy(event->name, vevent_ptr->name);

   printf("I am Master and I got cpu :%lf || nw:%lf || failed_calls:%d || response_time:%lf from %s \n", event->cpu_usage,event->network_usage,event->failed_calls,event->max_response_time,event->name);

   current_time = get_time();
   
    

    if(known_neighbours<dfg_obj->num_sources)
    {
        if (count==0)
        {
            char *neighbour=malloc(strlen(event->name));
            strcpy(neighbour,event->name);
            nodes_i_can_reach[0]=neighbour;
            known_neighbours=1;
            printf("I can reach %s\n",neighbour);
        }
        else
        {
            int match=0;
            for(i=0;i<known_neighbours;i++)
            { 
                if(strcmp(nodes_i_can_reach[i],event->name)==0)
                    match=1;

            }
            if(match==0)
            {
                char *neighbour=malloc(strlen(event->name));
                strcpy(neighbour,event->name);
                nodes_i_can_reach[known_neighbours]=neighbour;
                known_neighbours++;
                printf("I can reach %s\n",neighbour);


            }
        }  
    }

    server_call_rate = get_server_max_callrate();
    printf(" max call rate of the server : %f \n", server_call_rate);
    int prev_event_index = find_previous_event(event);
    

    if(server_call_rate >= 320 && loadbalancer == 0)
    {
	invoke_load_balancer_server();
	reroute_sipp_traffic();
	char heaviestclient[10] = find_heaviest_client();
	invoke_load_balancer_client(heaviest_client);
        loadbalancer=1;
   }

    if((current_time - action_timestamp) > 4.5)
    {
	if(event->failed_calls >0 || event->watchdog_major > 0 || event->watchdog_minor > 0)
	{
		printf(" time to adjust system parameters %d %d %d \n", event->failed_calls,event->watchdog_major,event->watchdog_minor);
		adjust_parameters_all_nodes(event);
					
	}
	else if(prev_event_index != -1)
	{
		if((event->max_response_time > QOS_THRESHOLD))
		{
			printf(" time to adjust system parameters because of response time : %d \n", event->max_response_time);
			adjust_parameters_all_nodes(event);	
		}
	}
	else 
	{
		;
	}
     }
	store_as_previous_event(event);
    
        
    //EVclient_shutdown(test_client, TRUE);
    
}
void print_to_file(char *contact)
{
    char node_name[50];
    char cmd[300];
    FILE *fp = fopen("master_contact.txt","w");
    if(fp!= NULL)
    {
        fprintf(fp,"%s",contact);
        fclose(fp);

    }
    
    fp = fopen("list_of_nodes.txt","r");
    if(fp == NULL)
        {
            printf("list_of_nodes.txt NOt Found\n");
            return;
         }   
        fscanf(fp,"%s",node_name);
        if(strlen(node_name)==0)
        return;
        while(node_name != EOF && strlen(node_name)>1)
        {
            strcpy(cmd,"rsync master_contact.txt ");
            strcat(cmd,node_name);
            strcat(cmd,":/home/ubuntu/build_area/evpath/ub14-64/examples");
            system(cmd);
            printf("%s\n",cmd);
            fscanf(fp,"%s",node_name);
            memset(cmd,0,sizeof(cmd));
            memset(node_name,0,sizeof(node_name));
            fscanf(fp,"%s",node_name);
        }  

}


void join_handler_func(EVmaster master, char *identifier, void*cur_unused1, void*cur_unused2)
{
    static int sources=0,middles=0,sink=0,control=0;
    EVdfg evdfg;
    EVmaster_assign_canonical_name(master, identifier, identifier);
    printf("joined : %s\n",identifier);
    //printf( "result :%d",strstr(identifier,"source"));
    if(strstr(identifier,"middle"))
    {
         middles++;
         dfg_node_middle *mid = malloc(sizeof(dfg_node_middle));
         mid->name=strdup(identifier);
         mid->control_source_type=strdup("control source");
         mid->control_handler=strdup("control_handler");
         mid->source_type=strdup("middle source");
         mid->sink_handler=strdup("middle_handler"); 
         mid->clink_count=0;
         dfg_obj->middles[middles-1]=mid;
    }
     
    else if(strstr(identifier,"control"))
    {
        printf("Control process joined:%s\n",identifier);
        control++;
    }      
    else if(strstr(identifier,"source"))
    {
        char* control_name=malloc(strlen(identifier)+10);
        sources++;  
         dfg_node_source *src = malloc(sizeof(dfg_node_source));
         src->control=malloc(sizeof(node_control_source));
         strcpy(control_name,identifier);
         strcat(control_name," control");
         src->control->name=strdup(control_name);
         printf("control name is %s\n",src->control->name);
         src->control->control_source_type=strdup("control source");
         src->name=strdup(identifier);
         src->source_type=strdup("event source");
         dfg_obj->sources[sources-1]=src;

    }
            
    else if(strstr(identifier,"sink"))
    {
      sink++;
      dfg_node_sink *sink = malloc(sizeof(dfg_node_sink));
         sink->name=strdup(identifier);
         sink->sink_handler=strdup("simple_handler");
         sink->control_source_type=strdup("control source");
         sink->clink_count=0;
         dfg_obj->sink[0]=sink;
        
    }
       printf("Sources: %d control: %d Middles: %d Sink: %d\n",sources,control,middles,sink);     
    if(sources< dfg_obj->num_sources||control<dfg_obj->num_sources||middles< dfg_obj->num_middles||sink<1)
        return;


    printf("\nFalling through and creating the DFG\n");
    evdfg = EVdfg_create(master);
    int it;
    for(it=0;it < dfg_obj->num_sources;it++)
    {
        EVdfg_stone src;
        EVdfg_stone control_sink;
        printf("Creating source stone for event source \n");
        src = EVdfg_create_source_stone(evdfg, "event source");
        control_sink = EVdfg_create_sink_stone(evdfg, "control_handler");
        EVdfg_assign_node(control_sink, dfg_obj->sources[it]->control->name);
        EVdfg_assign_node(src, dfg_obj->sources[it]->name);
        dfg_obj->sources[it]->source=src;
        dfg_obj->sources[it]->control->control_sink=control_sink;

    }

    for(it=0;it < dfg_obj->num_middles;it++)
    {
        EVdfg_stone src,sink,src2,sink2;
        printf("Creating source stone for middle  \n");
        src = EVdfg_create_source_stone(evdfg, "middle source");
        src2= EVdfg_create_source_stone(evdfg, "control source");
        EVdfg_assign_node(src, dfg_obj->middles[it]->name);
        EVdfg_assign_node(src2, dfg_obj->middles[it]->name);
        dfg_obj->middles[it]->source=src;
        dfg_obj->middles[it]->control_source=src2;

        printf("Creating sink stone for middle \n");
        sink = EVdfg_create_sink_stone(evdfg, "middle_handler");
        EVdfg_assign_node(sink, dfg_obj->middles[it]->name);
        dfg_obj->middles[it]->sink=sink;
        sink2 = EVdfg_create_sink_stone(evdfg, "control_handler");
        EVdfg_assign_node(sink2, dfg_obj->middles[it]->name);
        dfg_obj->middles[it]->control_sink=sink2;


    }

    EVdfg_stone final_sink,final_control_src;
    final_sink=EVdfg_create_sink_stone(evdfg, "simple_handler");
    EVdfg_assign_node(final_sink, dfg_obj->sink[0]->name);
    dfg_obj->sink[0]->sink=final_sink;
    final_control_src=EVdfg_create_source_stone(evdfg, "control source");
    EVdfg_assign_node(final_control_src, dfg_obj->sink[0]->name);
    dfg_obj->sink[0]->control_source=final_control_src;


    // linking procedure
    int i;
    
    double num_levels = levels;
    int middle_levels =(int) num_levels-2;
printf("Total nodes: %d, num_levels: %lf,middle_levels: %d\n",total_nodes,num_levels,middle_levels);  
    if(dfg_obj->num_sources <=2 && dfg_obj->num_middles==0)
    {
        for(i=0;i<dfg_obj->num_sources;i++)
        {
            EVdfg_link_port(dfg_obj->sources[i]->source,0,dfg_obj->sink[0]->sink);
            EVdfg_link_port(dfg_obj->sink[0]->control_source,dfg_obj->sink[0]->clink_count,dfg_obj->sources[i]->control->control_sink);
            dfg_obj->sink[0]->clink_count++;
        }
        
    }
    else
    {
        for(i=0;i<dfg_obj->num_sources;i++)
        {
            int link_index = i/2;
            if(link_index < dfg_obj->num_middles)
            {
            printf("leaf: Linking source %s stone with sink %s stone \n",dfg_obj->sources[i]->name,dfg_obj->middles[link_index]->name);
            EVdfg_link_port(dfg_obj->sources[i]->source,0,dfg_obj->middles[link_index]->sink);
            printf("leaf: Linking control source %s to control sink %s\n",dfg_obj->middles[link_index]->name,dfg_obj->sources[i]->control->name);
            EVdfg_link_port(dfg_obj->middles[link_index]->control_source,dfg_obj->middles[link_index]->clink_count,dfg_obj->sources[i]->control->control_sink);
            dfg_obj->middles[link_index]->clink_count++;
            }
           
        }
        int cur_level_nodes=(dfg_obj->num_sources+1)/2;
        int prev_level_nodes=0;
        int j=0;
        for(i=0;i<middle_levels-1;i++)
        {
            //int link_index = i/2;

            for(j=0;j<cur_level_nodes;j++)
            {   
                int link_index=cur_level_nodes+(j/2);
                int link_index1=j+prev_level_nodes;
                if(link_index1>=dfg_obj->num_middles||link_index>=dfg_obj->num_middles)
                    {
                        printf("Not enough middles\n");
                        return;
                    }
                printf("middle: Linking middle source %s stone with middle sink %s stone \n",dfg_obj->middles[link_index1]->name,dfg_obj->middles[link_index]->name);
                EVdfg_link_port(dfg_obj->middles[link_index1]->source,0,dfg_obj->middles[link_index]->sink);
                EVdfg_link_port(dfg_obj->middles[link_index]->control_source,dfg_obj->middles[link_index]->clink_count,dfg_obj->middles[link_index1]->control_sink);
                dfg_obj->middles[link_index]->clink_count++;
            }
            prev_level_nodes=cur_level_nodes;
            cur_level_nodes=(cur_level_nodes+1)/2;

            
         }
        int last = dfg_obj->num_middles-1;
        EVdfg_link_port(dfg_obj->middles[last]->source,0,dfg_obj->sink[0]->sink);
        EVdfg_link_port(dfg_obj->sink[0]->control_source,dfg_obj->sink[0]->clink_count,dfg_obj->middles[last]->control_sink);
        dfg_obj->sink[0]->clink_count++;
        if(dfg_obj->num_middles>=2)
        {
		printf("sink: Linking sink control source stone with middle control sink stone\n");
            EVdfg_link_port(dfg_obj->middles[last-1]->source,0,dfg_obj->sink[0]->sink);
            EVdfg_link_port(dfg_obj->sink[0]->control_source,1,dfg_obj->middles[last-1]->control_sink);    
        }
        



    }
    

    EVdfg_realize(evdfg);
}

int main(int argc, char **argv)
{
    CManager cm;
    char *str_contact;
    EVmaster test_master;
    EVclient_sinks sink_capabilities;

    (void)argc; (void)argv;
    cm = CManager_create();
    CMlisten(cm);
    test_master = EVmaster_create(cm);
    str_contact = EVmaster_get_contact_list(test_master);
    EVmaster_node_join_handler (test_master, join_handler_func);

    print_to_file(str_contact);
    printf("Contact list is \"%s\"\n", str_contact);

    dfg_obj = malloc(sizeof(dfg));
    dfg_obj->num_sources=atoi(argv[1]);
    int val=0;
    levels=0;
    total_nodes=dfg_obj->num_sources;
    val=total_nodes;
    while(val>1)
    {
    levels++;
    val=(val+1)/2;
    total_nodes+=val;

    }

    levels++; 	
  			
    dfg_obj->num_middles=total_nodes-dfg_obj->num_sources-1;
    printf("DFG Stats total_nodes %d, middles %d, levels %d",total_nodes,dfg_obj->num_middles,levels);	
    dfg_obj->sources= (dfg_node_source**)malloc(sizeof(dfg_node_source*)*dfg_obj->num_sources);
    dfg_obj->sink= (dfg_node_sink**)malloc(sizeof(dfg_node_sink*)*1);
    dfg_obj->middles= (dfg_node_middle**)malloc(sizeof(dfg_node_middle*)*dfg_obj->num_middles);
    
    source_handle=EVcreate_submit_handle(cm, -1, simple_control_format_list);
    source_capabilities = EVclient_register_source("control source", source_handle);
    sink_capabilities = EVclient_register_sink_handler(cm, "simple_handler", simple_format_list,
                (EVSimpleHandlerFunc) simple_handler, NULL);
    /* We're node "a" in the DFG */
    test_client = EVclient_assoc_local(cm, "sink", test_master, source_capabilities, sink_capabilities);
    nodes_i_can_reach=malloc(sizeof(char*)*dfg_obj->num_sources);
    
    if (EVclient_ready_wait(test_client) != 1) {
    /* dfg initialization failed! */
    exit(1);
    }


    if (EVclient_active_sink_count(test_client) > 0) {
    /* if there are active sinks, the handler will call EVclient_shutdown() */
    } else {
    if (EVclient_source_active(source_handle)) {
        /* we had a source and have already submitted, indicate success */
        EVclient_shutdown(test_client, 0 /* success */);
    } else {
        /* we had neither a source or sink, ready to shutdown, no opinion */
        EVclient_ready_for_shutdown(test_client);
    }
    }
    
    return(EVclient_wait_for_shutdown(test_client));

    
}
